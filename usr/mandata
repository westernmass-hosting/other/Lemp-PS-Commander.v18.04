#!/bin/bash

_c=(dialog --clear --backtitle "GYOC Database Manager" --title "GYOC Database Manager" --menu "What would you like to do?" 0 0 9)
_opts=(1 "Backup a Database"
		 2 "Restore a Database"
		 3 "Create a Database"
		 4 "Delete a Database"
		 5 "Add Database User"
		 6 "Delete Database User"
		 7 "Change Database Password"
		 8 "Change the Master Password"
		 99 "BACK")
_choices=$("${_c[@]}" "${_opts[@]}" 2>&1 >/dev/tty)
# get our master mysql credentials
MUN=$(cat /usr/bin/templates/.mucnf);
MPW=$(cat /usr/bin/templates/.mpcnf);
for _choice in $_choices; do
	case $_choice in
		1)
			# backup database
			input1=$(dialog --title "Database Backup" \
				--inputbox  "Please type in the account name." 0 0 3>&1 1>&2 2>&3 3>&-)
			if [[ -z $input1 ]]; then
				dialog --title "Backup Failure" --infobox "You will need to pass the Account before you can do this." 0 0
				sleep 3;
			else
				W=$(mysql -u $MUN -p"$MPW" -N information_schema -e "SELECT DISTINCT(TABLE_SCHEMA) FROM tables WHERE TABLE_SCHEMA LIKE '$input1%'");
				input2=$(dialog --title "Database Backup" \
					--inputbox  "Account: $input1 has the following databases:\n$W\nPlease type the name following the _ to back it up." 0 0 3>&1 1>&2 2>&3 3>&-)
				day=$(date +%F);
				db="${input1}_${input2}"
				buloc="/home/$input1/backups/database/$day-$input1.sql.gz"
				mysqldump --single-transaction -h localhost -u $MUN -p"$MPW" "$db" --routines --no-create-db | gzip > $buloc
				dialog --title "Backed up: $db" --infobox "Backup file is: $buloc" 0 0
				sleep 3;
			fi;
			;;
		2)
			# restore database
			dialog --keep-tite --clear --title "Restore Database" \
						--yesno "Do you need to create a new database to restore to?" 0 0
			if [ $? -eq 0 ]; then
				#yes - need name, password, and date to restore from
				input0=$(dialog --title "Database Restore" \
					--inputbox  "Please type in the account name." 0 0 3>&1 1>&2 2>&3 3>&-)
				input1=$(dialog --title "Database Restore" \
					--inputbox  "Please type in the database name. (will be prefixed with ${input0}_)" 0 0 3>&1 1>&2 2>&3 3>&-)
				input2=$(dialog --title "Database Restore" \
					--inputbox  "Please type in the new username for this database. (will be prefixed with ${input0}_)" 0 0 3>&1 1>&2 2>&3 3>&-)
				input3=$(dialog --title "Database Restore" \
					--passwordbox  "Please type in the new databases password." 0 0 3>&1 1>&2 2>&3 3>&-)
				input4=$(dialog --title "Database Restore" \
					--inputbox  "Please type in the date to restore from YYYY-MM-DD." 0 0 3>&1 1>&2 2>&3 3>&-)
				db="${input0}_${input1}"
				uname="${input0}_${input2}"
				# create the new database, with the user
				mysql -u $MUN -p"$MPW" -e "CREATE DATABASE $db";
				mysql -u $MUN -p"$MPW" -e "CREATE USER '$uname'@'localhost' IDENTIFIED BY '$input3'";
				mysql -u $MUN -p"$MPW" -e "GRANT ALL PRIVILEGES ON $db.* TO '$uname'@'localhost'";
				mysql -u $MUN -p"$MPW" -e "FLUSH PRIVILEGES";
				# now dump this info into the account
				echo $uname | base64 > /home/$input0/.acct/.dbu;
				echo $input3 | base64 > /home/$input0/.acct/.dbp;
				echo $db | base64 > /home/$input0/.acct/.dbn;
				# now restore the backup
				bu="/home/$input0/backups/database/${input4}-${input0}.sql.gz"
				gunzip -k $bu
				mysql -u $MUN -p"$MPW" -h localhost $db < /home/$input0/backups/database/*.sql;
				rm -f /home/$input0/backups/database/*.sql
				dialog --title "Database: $db" --infobox "Database is now restored." 0 0
				sleep 3;
			else
				# need name and date to restore from
				input0=$(dialog --title "Database Restore" \
					--inputbox  "Please type in the account name." 0 0 3>&1 1>&2 2>&3 3>&-)
				input1=$(dialog --title "Database Restore" \
					--inputbox  "Please type in the database name. (will be prefixed with ${input0}_)" 0 0 3>&1 1>&2 2>&3 3>&-)
				input2=$(dialog --title "Database Restore" \
					--inputbox  "Please type in the date to restore from YYYY-MM-DD." 0 0 3>&1 1>&2 2>&3 3>&-)
				db="${input0}_${input1}"
				bu="/home/$input0/backups/database/${input2}-${input0}.sql.gz"
				gunzip -k $bu
				mysql -u $MUN -p"$MPW" -h localhost $db < /home/$input0/backups/database/*.sql;
				rm -f /home/$input0/backups/database/*.sql
				dialog --title "Database: $db" --infobox "Database is now restored." 0 0
				sleep 3;
			fi;
			;;
		3)
			# create database
			input0=$(dialog --title "Database Creation" \
				--inputbox  "Please type in the account name." 0 0 3>&1 1>&2 2>&3 3>&-)
			input1=$(dialog --title "Database Creation" \
				--inputbox  "Please type in the database name. (will be prefixed with ${input0}_)" 0 0 3>&1 1>&2 2>&3 3>&-)
			input2=$(dialog --title "Database Creation" \
				--inputbox  "Please type in the new username for this database. (will be prefixed with ${input0}_)" 0 0 3>&1 1>&2 2>&3 3>&-)
			input3=$(dialog --title "Database Creation" \
				--passwordbox  "Please type in the new databases password." 0 0 3>&1 1>&2 2>&3 3>&-)
			db="${input0}_${input1}"
			uname="${input0}_${input2}"
			# create the new database, with the user
			mysql -u $MUN -p"$MPW" -e "CREATE DATABASE $db";
			mysql -u $MUN -p"$MPW" -e "CREATE USER '$uname'@'localhost' IDENTIFIED BY '$input3'";
			mysql -u $MUN -p"$MPW" -e "GRANT ALL PRIVILEGES ON $db.* TO '$uname'@'localhost'";
			mysql -u $MUN -p"$MPW" -e "FLUSH PRIVILEGES";
			echo $uname | base64 > /home/$input0/.acct/.dbu;
			echo $input3 | base64 > /home/$input0/.acct/.dbp;
			echo $db | base64 > /home/$input0/.acct/.dbn;
			dialog --title "Database: $db" --infobox "Database is now created with the user credentials you have specified." 0 0
			sleep 3;
			;;
		4)
			# delete database
			dialog --keep-tite --clear --title "Delete Database" \
						--yesno "Please make sure you have created a backup, and hit yes to continue." 0 0
			if [ $? -eq 0 ]; then
				#yes
				input0=$(dialog --title "Database Deletion" \
					--inputbox  "Please type in the account name." 0 0 3>&1 1>&2 2>&3 3>&-)
				input1=$(dialog --title "Database Deletion" \
					--inputbox  "Please type in the database name. (will be prefixed with ${input0}_)" 0 0 3>&1 1>&2 2>&3 3>&-)
				db="${input0}_${input1}"
				DBU=$(cat /home/$input0/.acct/.dbu | base64 -d)
				mysql -u $MUN -p"$MPW" -e "DROP DATABASE $db";
				mysql -u $MUN -p"$MPW" -e "DROP USER '$DBU'@'localhost'"
				mysql -u $MUN -p"$MPW" -e "FLUSH PRIVILEGES"
				dialog --title "Database: $db" --infobox "The database $db has been deleted." 0 0
				sleep 3;
			else
				sleep 1;
			fi;
			;;
		5)
			# add user
			input0=$(dialog --title "Database Add User" \
				--inputbox  "Please type in the account name." 0 0 3>&1 1>&2 2>&3 3>&-)
			input1=$(dialog --title "Database Add User" \
				--inputbox  "Please type in the new user name. (will be prefixed with ${input0}_)" 0 0 3>&1 1>&2 2>&3 3>&-)
			input2=$(dialog --title "Database Add User" \
				--passwordbox  "Please type in the new users password." 0 0 3>&1 1>&2 2>&3 3>&-)
			input4=$(dialog --title "Database Add User" \
				--inputbox  "Please type in the database name. (will be prefixed with ${input0}_)" 0 0 3>&1 1>&2 2>&3 3>&-)
			db="${input0}_${input4}"
			uname="${input0}_${input1}"
			mysql -u $MUN -p"$MPW" -e "CREATE USER '$uname'@'localhost' IDENTIFIED BY '$input2'";
			mysql -u $MUN -p"$MPW" -e "GRANT ALL PRIVILEGES ON $db.* TO '$uname'@'localhost'";
			mysql -u $MUN -p"$MPW" -e "FLUSH PRIVILEGES";
			echo $uname | base64 > /home/$input0/.acct/.dbu;
			echo $input2 | base64 > /home/$input0/.acct/.dbp;
			dialog --title "Database: $db" --infobox "The new user has been created in database: $db." 0 0
			sleep 3;
			;;
		6)
			# delete a user
			input0=$(dialog --title "Database Delete User" \
				--inputbox  "Please type in the account name." 0 0 3>&1 1>&2 2>&3 3>&-)
			input1=$(dialog --title "Database Delete User" \
				--inputbox  "Please type in the users name. (will be prefixed with ${input0}_)" 0 0 3>&1 1>&2 2>&3 3>&-)
			uname="${input0}_${input1}"
			mysql -u $MUN -p"$MPW" -e "DROP USER '$uname'@'localhost'"
			mysql -u $MUN -p"$MPW" -e "FLUSH PRIVILEGES"
			dialog --title "Database User Delete" --infobox "The user $uname has been removed." 0 0
			sleep 3;
			;;
		7)
			# change a users password
			input0=$(dialog --title "Database Change Password" \
				--inputbox  "Please type in the account name." 0 0 3>&1 1>&2 2>&3 3>&-)
			input1=$(dialog --title "Database Change Password" \
				--inputbox  "Please type in the users name. (will be prefixed with ${input0}_)" 0 0 3>&1 1>&2 2>&3 3>&-)
			input2=$(dialog --title "Database Change Password" \
				--passwordbox  "Please type in the users new password." 0 0 3>&1 1>&2 2>&3 3>&-)
			uname="${input0}_${input1}"
			echo $input2 | base64 > /home/$input0/.acct/.dbp;
			mysql -u $MUN -p"$MPW" -e "ALTER USER '$uname'@'localhost' IDENTIFIED BY '$input2'"
			mysql -u $MUN -p"$MPW" -e "FLUSH PRIVILEGES"
			dialog --title "Database Password Change" --infobox "The users password has been changed." 0 0
			sleep 3;
			;;
		8)
			# change master password
			input2=$(dialog --title "Database Master Password" \
				--passwordbox  "Please type in the new password." 0 0 3>&1 1>&2 2>&3 3>&-)
			mysql -u $MUN -p"$MPW" -e "ALTER USER '$MUN'@'localhost' IDENTIFIED BY '$input2'"
			mysql -u $MUN -p"$MPW" -e "FLUSH PRIVILEGES"
			echo $input2 > /usr/bin/templates/.mpcnf;
			dialog --title "Master Password Change" --infobox "The master password has been changed." 0 0
			sleep 3;
			;;
		99|*)
			break;
			;;
	esac;
done;

